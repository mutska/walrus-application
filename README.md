# Walrus Image Filter Application

## Descripción

Filtro de imágenes.


Este programa de software fue construido durante el semestre 2022-2 de la facultad de ciencias UNAM
para el curso de proceso digital de imágenes.

Gracias a la morsa :)

## Lista de tareas terminadas

- [x] Tarea 1 (Filtros Básicos)
  - [x] Filtro Gris
  - [x] Filtro Rojo
  - [x] Filtro Verde
  - [x] Filtro Azul
  - [x] Filtro Mosaico
  - [x] Filtro Alto Constraste
  - [x] Filtro Inverso
  - [x] Filtro Brillo
- [x] Tarea 2 (Filtros De Convolución)
  - [x] Filtro Blur
  - [x] Filtro Motion Blur
  - [x] Filtro Bordes en vertical
  - [x] Filtro Bordes en horizontal
  - [x] Filtro Bordes a 45 grados
  - [x] Filtro Bordes en todas las direcciones
  - [x] Filtro Sharpen
  - [x] Filtro Emboss
  - [x] Filtro Promedio
  - [x] Filtro Mediana
  - [x] Filtro RGB
- [x] Tarea 3 (Filtros con letras)
  - [x] Una Letra con Color
  - [x] Una Letra en Gris
  - [x] Letras en Blanco y Negro
  - [x] Letras en Color
  - [x] Letras en Gris
  - [x] Texto Personal a color
  - [x] Naipes
  - [x] Domino (fichas blancas)
  - [x] Domino (fichas negras)
- [ ] Tarea 4 (Marca de agua)
- [x] Tarea 5 (Esteganografía)
  - [x] Codificar Texto
  - [x] Decodificar Texto
- [x] Tarea 6 (Imágenes Recursivas)
  - [x] Tonos de Gris
- [x] Tarea 7 (Filtro Óleo)
  - [x] Tonos de Gris
- [x] Tarea 8 (Filtro Alto Contraste Automático)
  - [x] Tonos de Gris
- [x] Tarea 9 (Dithering)
  - [x] Ordenado
- [x] Tarea 10 (Fotomosaico)
  - [x] Estándar  

## Instrucciones de Instalación

Descargamos el proyecto 

1. Instalar python (versión >= 3.9.10) y abrir una terminal de comandos en la raíz del proyecto.

2. Usar el gestor de paquetes *pip* (que viene con python) e instalar el software necesario con el siguiente comando:  `pip install -r requirements.txt`

3. Ejecutar la aplicación con el siguiente comando: `python run.py`

## Construcción de un ejecutable

Si queremos construir un ejecutable en formato *.exe* necesitamos instalar *pyinstaller*: `pip install pyinstaller`

Luego ejecutamos *pyinstaller* con los siguientes parámetros:

`pyinstaller --windowed --noconsole --onefile --name walrusfilter .\run.py`

El ejecutable lo podremos encontrar dentro de la carpeta *dist*.

## Comentarios

- La aplicación muestra un mensaje que indica cuando se termino de aplicar un filtro o termino una acción, si no muestra nada es porque no ha terminado la acción correspondiente.

- Sobre el filtro de esteganografía, al codificar el texto este se guarda en la imagen que en ese momento este cargada en el canvas, luego tendríamos que guardar la imagen donde nosotros queramos. Además la decodificación del texto oculto se hace sobre la imagen que se encuentre en ese momento en el canvas y se abre una ventana para guardar el archivo *.txt* con el texto decodificado.

- Para los filtros *Imagenes recursivas* y *Fotomosaico* hay una botón con la leyenda: **Directorio con reglas e imagenes preprocesadas**, este botón espera que seleccionemos una carpeta o directorio donde ya se encuentran los archivos *.txt*  y las imágenes(prepreocesadas si asi se requiere) necesarias según el filtro que ocupemos, esto para no tener que empezar el preproceso desde cero. Si empezamos desde cero ocuparemos los botones *Tonos de gris* para *Imagenes recursivas* y *Estandar* para  *Fotomosaico*.

## Autor

By Marco Antonio Velasco Flores - also known as [mutska](https://www.mutska.tech).



