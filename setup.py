from setuptools import setup, find_packages

setup(
    name='WalrusFilters',
    version='1.0',
    author='Marco Antonio Velasco Flores',
    author_email='mutska@protonmail.com',
    description='Simple image filter application',
    #url="http://qtictactoe.example.com",
    license='MIT',
    long_description=open("README.md", 'r').read(),
    keywords='image filter pyqt5',
    project_urls={
        'Author Website': 'https://www.mutska.tech',
        'Source Code': 'https://gitlab.com/mutska/walrus-application',
    },
    packages=find_packages(),
    install_requires=['PyQt5 >= 5.15', 'screeninfo >= 0.8'],
    python_requires='>=3.6',
    package_data={
        'walrusfilter.images': ['*.png'],
        '': ['*.txt', '*.md']
    },
    entry_points={
        'console_scripts': [
            'walrusfilter = walrusfilter.__main__:main'
        ]
    }
)
