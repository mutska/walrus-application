from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from .CustomDialogs import *
import enum
import statistics

class Filter(enum.Enum):
    Gray = 0
    Inverse = 1
    HighContrast = 2
    Red = 3
    Green = 4
    Blue = 5
    Mosaic = 6
    Brightness = 7
    HardBlur = 8
    MotionBlur = 9
    HorizontalEdges = 10
    VerticalEdges = 11
    DegreesEdges = 12
    AllEdges = 13
    Sharpen = 14
    HardSharpen = 15
    Emboss = 16
    Mean = 17
    Median = 18
    RGB = 19
    ONELC = 20
    ONELG = 21
    LETTERSWB = 22
    LETTERSC = 23
    LETTERSG = 24
    TEXTC = 25
    CARDS = 26
    WDOMINO = 27
    BDOMINO = 28
    TEXTENC = 29
    TEXTDEC = 30
    OLEO = 31
    CONTRASTAUTO = 32
    CLUSTERDITH = 33
    PHOTOMOSAIC = 34
    PHOTOMOSAICDIR = 35
    RECURSIVE = 36
    RECURSIVEDIR = 37
    PREPROCESSING01 = 38
    PREPROCESSING02 = 39


def oleo_filter(pixmap: qtg.QPixmap):
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            color  = qtg.QColor(gray, gray, gray,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    copy = pixmap.copy().toImage()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)


    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            pixels = []            
    
            # 1 Pixel (0, 0)
            # g, _, _, _ = image.pixelColor(i, j).getRgb()
            # pixels.append(g)

            # 2 Pixel (0, -1)
            g, _, _, _ = image.pixelColor(i - 1, j).getRgb()
            pixels.append(g)


            # 3 Pixel (0, -2)
            g, _, _, _ = image.pixelColor(i - 2, j).getRgb()
            pixels.append(g)


            # 4 Pixel (0, 1)
            g, _, _, _ = image.pixelColor(i + 1, j).getRgb()
            pixels.append(g)


            # 5 Pixel (0, 2)
            g, _, _, _ = image.pixelColor(i + 2, j).getRgb()
            pixels.append(g)


            # 6 Pixel (-1, 0)
            g, _, _, _ = image.pixelColor(i, j - 1).getRgb()
            pixels.append(g)


            # 7 Pixel (-2, 0)
            g, _, _, _ = image.pixelColor(i, j - 2).getRgb()
            pixels.append(g)


            # 8 Pixel (1, 0)
            g, _, _, _ = image.pixelColor(i, j + 1).getRgb()
            pixels.append(g)


            # 9 Pixel (2, 0)
            g, _, _, _ = image.pixelColor(i, j + 2).getRgb()
            pixels.append(g)


            # 10 Pixel (-1, -1)
            g, _, _, _ = image.pixelColor(i - 1, j - 1).getRgb()
            pixels.append(g)


            # 11 Pixel (-2, -2)
            g, _, _, _ = image.pixelColor(i - 2, j - 2).getRgb()
            pixels.append(g)


            # 12 Pixel (1, 1)
            g, _, _, _ = image.pixelColor(i + 1, j + 1).getRgb()
            pixels.append(g)


            # 13 Pixel (2, 2)
            g, _, _, _ = image.pixelColor(i + 2, j + 2).getRgb()
            pixels.append(g)


            # 14 Pixel (-1, 1)
            g, _, _, _ = image.pixelColor(i + 1, j - 1).getRgb()
            pixels.append(g)


            # 15 Pixel (-2, 2)
            g, _, _, _ = image.pixelColor(i + 2, j - 2).getRgb()
            pixels.append(g)


            # 16 Pixel (1, -1)
            g, _, _, _ = image.pixelColor(i - 1, j + 1).getRgb()
            pixels.append(g)


            # 17 Pixel (2, -2)
            g, _, _, _ = image.pixelColor(i - 2, j + 2).getRgb()
            pixels.append(g)


            # 18 Pixel (-1, -2)
            g, _, _, _ = image.pixelColor(i - 2, j - 1).getRgb()
            pixels.append(g)


            # 19 Pixel (1, -2)
            g, _, _, _ = image.pixelColor(i - 2, j + 1).getRgb()
            pixels.append(g)


            # 20 Pixel (-1, 2)
            g, _, _, _ = image.pixelColor(i + 2, j - 1).getRgb()
            pixels.append(g)


            # 21 Pixel (1, 2)
            g, _, _, _ = image.pixelColor(i + 2, j - 1).getRgb()
            pixels.append(g)


            # 22 Pixel (-2, -1)
            g, _, _, _ = image.pixelColor(i - 1, j - 2).getRgb()
            pixels.append(g)


            # 23 Pixel (2, -1)
            g, _, _, _ = image.pixelColor(i - 1, j + 2).getRgb()
            pixels.append(g)


            # 24 Pixel (-2, 1)
            g, _, _, _ = image.pixelColor(i + 1, j - 2).getRgb()
            pixels.append(g)

            # 25 Pixel (2, 1)
            g, _, _, a = image.pixelColor(i + 1, j + 2).getRgb()
            pixels.append(g)

            gray = statistics.mode(pixels)
            color  = qtg.QColor(gray, gray, gray,a)
            copy.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(copy)
    return pixmap

def auto_contrast_filter(pixmap: qtg.QPixmap):
    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    frequency_grays = {}

    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            color  = qtg.QColor(gray, gray, gray,a)
            image.setPixelColor(i,j, color)
            if gray in frequency_grays:
                frequency_grays[gray] += 1
            else:
                frequency_grays[gray] = 1

    maximum_gray = -1
    minimum_gray = 300

    maximum_value = -1
    minimum_value = 10000000

    for key, value in frequency_grays.items():
        if maximum_value < value:
            maximum_value = value
            maximum_gray = key
        if minimum_value > value:
            minimum_value = value
            minimum_gray = key

    maximum_contrast = abs(maximum_gray - minimum_gray)

    for i in range(width):
        for j in range(height):                    
            gray, _, _, a = image.pixelColor(i,j).getRgb()
            new_gray = (gray // maximum_contrast) * 255
            if new_gray > 255:
                new_gray = 255
            color  = qtg.QColor(new_gray, new_gray, new_gray,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def gray_filter(pixmap: qtg.QPixmap):
    image = pixmap.toImage()
    width = image.width()
    height = image.height()



    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            color  = qtg.QColor(gray, gray, gray,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def inverse_filter(pixmap: qtg.QPixmap):
    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            hg = 0 if gray > 127 else 255
            color  = qtg.QColor(hg, hg, hg,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def high_contrast_filter(pixmap: qtg.QPixmap):
    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            hg = 255 if gray > 127 else 0
            color  = qtg.QColor(hg, hg, hg,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def red_filter(pixmap: qtg.QPixmap):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            color  = qtg.QColor(r, 0, 0,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def green_filter(pixmap: qtg.QPixmap):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            color  = qtg.QColor(0, g, 0,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def blue_filter(pixmap: qtg.QPixmap):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            color  = qtg.QColor(0, 0, b,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def mosaic_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    if settings["width"] == 0 or settings["height"] == 0:
        return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
        return pixmap


    mosaic_width = settings["width"]
    mosaic_height = settings["height"]


    for i in range(0, width, mosaic_width):
        for j in range(0, height, mosaic_height):
            r_average = 0
            g_average = 0
            b_average = 0
            size = 0
            alpha = 0
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        size += 1
                        r, g, b, alpha = image.pixelColor(k,l).getRgb()
                        r_average += r;
                        g_average += g;
                        b_average += b;
            r_average = r_average // size
            g_average = g_average // size
            b_average = b_average // size
            color  = qtg.QColor(r_average, g_average, b_average ,alpha)
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        image.setPixelColor(k,l, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def cluster_dithering_filter(pixmap: qtg.QPixmap):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            color  = qtg.QColor(gray, gray, gray,a)
            image.setPixelColor(i,j, color)

    mosaic_width  = 3
    mosaic_height = 3

    metric = 28
    for i in range(0, width, mosaic_width):
        for j in range(0, height, mosaic_height):
            pos = 0
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        gray, _, _, _ = image.pixelColor(k,l).getRgb()
                        pixel_color = get_pixel_color(pos, gray, metric)
                        color  = qtg.QColor(pixel_color, pixel_color, pixel_color, 0)
                        image.setPixelColor(k,l, color)
                        pos += 1


    pixmap = qtg.QPixmap(image)
    return pixmap

def get_pixel_color(pos, gray, metric):
    black = 0
    white = 255
    division = gray // metric
    if pos == 0:
        return white if division >= 8 else black
    if pos == 1:
        return white if division >= 3 else black
    if pos == 2:
        return white if division >= 4 else black
    if pos == 3:
        return white if division >= 6 else black
    if pos == 4:
        return white if division >= 1 else black
    if pos == 5:
        return white if division >= 2 else black
    if pos == 6:
        return white if division >= 7 else black
    if pos == 7:
        return white if division >= 5 else black
    if pos == 8:
        return white if division >= 9 else black


def brightness_filter(pixmap: qtg.QPixmap, settings:  dict):


    brightness = settings["brightness"]

    if brightness == 0:
        return pixmap

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            rr = r + brightness
            gg = g + brightness
            bb = b + brightness
            if rr < 0: rr = 0
            elif rr > 255: rr = 255
            if gg < 0: gg = 0
            elif gg > 255: gg = 255
            if bb < 0: bb = 0
            elif bb > 255: bb = 255
            color  = qtg.QColor(rr, gg, bb ,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap

def rgb_filter(pixmap: qtg.QPixmap, settings: dict):


    if not settings["check"]:
        return pixmap
    image = pixmap.toImage()
    width = image.width()
    height = image.height()
    rgb_red = settings["red"]
    rgb_green = settings["green"]
    rgb_blue = settings["blue"]


    for i in range(width):
        for j in range(height):

            r, g, b, a = image.pixelColor(i,j).getRgb()

            red = r & rgb_red
            green = g & rgb_green
            blue = b & rgb_blue

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue ,a)
            image.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(image)
    return pixmap
