from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
import os
import sys
from math import dist

def get_rgb_config(full_source):
    try: 
        image = qtg.QImage(full_source)
        width = image.width()
        height = image.height()

        red_sum = 0
        green_sum = 0
        blue_sum = 0
        size = width * height

        for i in range(width):
            for j in range(height):                    
                r, g, b, _ = image.pixelColor(i,j).getRgb()
                red_sum += r
                green_sum += g
                blue_sum += b

        red_sum = red_sum // size
        green_sum = green_sum // size
        blue_sum = blue_sum // size
        line = f"{red_sum} {green_sum} {blue_sum} {full_source}\n" 
        return line
    except:
        return "bad"

def get_closest_image(images_source, r, g, b):
    configuration = open(images_source, "r", encoding="utf-8")
    best_image_source = ""
    minimum = sys.float_info.max
    for line in configuration:
        values = line.strip().split()
        red = int(values[0])
        green = int(values[1])
        blue = int(values[2])
        image = values[3]
        x = (r, g, b)
        y = (red, green, blue)
        distance = dist(x, y)
        if distance < minimum:
            minimum = distance
            best_image_source = image
    configuration.close()
    return best_image_source

def step_one_preprocessing(pixmap: qtg.QPixmap):

    options = qtw.QFileDialog.Options()
    options |= qtw.QFileDialog.ShowDirsOnly

    dir_path = qtw.QFileDialog.getExistingDirectory(None, "ESCOGER DIRECTORIO DE IMAGENES", options=options)
    rgb_means_source = dir_path + "/" + "rgbs_promedio.txt"


    rgb_means = open(rgb_means_source, "w+", encoding="utf-8")
    not_atleast_one_jpg = True
    for file in os.listdir(dir_path):
        if file.endswith(".JPG") or file.endswith(".jpg"):
            not_atleast_one_jpg = False
            full_source = dir_path + "/" + file
            line = get_rgb_config(full_source)
            if line == "bad":
                continue
            rgb_means.write(line)

    if not_atleast_one_jpg:
        rgb_means.write("NOIMAGE")
    rgb_means.close()
    msgBox = qtw.QMessageBox()
    msgBox.setIcon(qtw.QMessageBox.Information)
    msgBox.setText("rbgs_promedio.txt construido\nEn el paso 2 selecciona este archivo.")
    msgBox.setStandardButtons(qtw.QMessageBox.Ok)
    value = msgBox.exec()
    return pixmap

def step_two_preprocessing(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    if settings["width"] == 0 or settings["height"] == 0:
         return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
         return pixmap

    mosaic_width = settings["width"]
    mosaic_height = settings["height"]
    mosaic_image_width = settings["mosaic_width"]
    mosaic_image_height = settings["mosaic_height"]

    info = qtw.QFileDialog.getOpenFileName(None, "ESCOGER rbgs_promedio.txt", filter="Text files (*.txt)")
    rgb_means_source = info[0]
    dir_path = rgb_means_source.rpartition('/')[0]


    constructor_source = dir_path + "/" + "constructor.txt"
    metrics_source = dir_path + "/" + "metrics.txt"

    mosaic_area = mosaic_height * mosaic_width
    constructor = open(constructor_source, "w+", encoding="utf-8")
    for j in range(0, height, mosaic_height):
        for i in range(0, width, mosaic_width):
            if j + mosaic_height <= height and i + mosaic_width <= width:
                r_average = 0
                g_average = 0
                b_average = 0
                for k in range(i, i + mosaic_width, 1):
                    for l in range(j, j + mosaic_height, 1):
                        r, g, b, _ = image.pixelColor(k,l).getRgb()
                        r_average += r
                        g_average += g
                        b_average += b
                r_average = r_average // mosaic_area
                g_average = g_average // mosaic_area
                b_average = b_average // mosaic_area
                best_image_source = get_closest_image(rgb_means_source,
                        r_average,
                        g_average,
                        b_average)
                constructor.write(f"{best_image_source}\n")

    constructor.close()

    metrics = open(metrics_source, "w+", encoding="utf-8")
    metrics.write(f"{width}\n")
    metrics.write(f"{height}\n")
    metrics.write(f"{mosaic_width}\n")
    metrics.write(f"{mosaic_height}\n")
    metrics.write(f"{mosaic_image_width}\n")
    metrics.write(f"{mosaic_image_height}\n")
    metrics.close()

    msgBox = qtw.QMessageBox()
    msgBox.setIcon(qtw.QMessageBox.Information)
    msgBox.setText("""metrics.txt construido.\nconstructor.txt construido.\nAmbos archivos estan en la misma carpeta que rgbs_promedio.txt\n\nEn el paso 3 selecciona la carpeta que contiene ambos archivos.""")
    msgBox.setStandardButtons(qtw.QMessageBox.Ok)
    value = msgBox.exec()
    return pixmap


def photomosaic_filter(pixmap: qtg.QPixmap):

    options = qtw.QFileDialog.Options()
    options |= qtw.QFileDialog.ShowDirsOnly
    dir_path = qtw.QFileDialog.getExistingDirectory(None, "Directorio de reglas e imagenes preprocesadas", options=options)

    constructor_source = dir_path + "/" + "constructor.txt"
    metrics_source = dir_path + "/" + "metrics.txt"

    if os.path.exists(constructor_source) == False or os.path.exists(metrics_source) == False:
        return pixmap

    if not any((file.endswith(".JPG") or file.endswith(".jpg")) for file in os.listdir(dir_path)):
        return pixmap


    metrics = open(metrics_source, "r", encoding="utf-8")
    metrics_list = metrics.readlines()
    metrics.close()

    width = int(metrics_list[0].strip())
    height = int(metrics_list[1].strip())

    mosaic_width = int(metrics_list[2].strip())
    mosaic_height = int(metrics_list[3].strip())

    mosaic_image_width = int(metrics_list[4].strip())
    mosaic_image_height = int(metrics_list[5].strip())


    y_mosaics = height // mosaic_height
    x_mosaics = width // mosaic_width

    final_image_width = x_mosaics * mosaic_image_width
    final_image_height = y_mosaics * mosaic_image_height
    final_image = qtg.QImage(qtc.QSize(final_image_width, final_image_height), qtg.QImage.Format_ARGB32)

    constructor = open(constructor_source, "r", encoding="utf-8")
    images = constructor.readlines()
    constructor.close()
    index = -1

    for j in range(0, final_image_height, mosaic_image_height):
        for i in range(0, final_image_width, mosaic_image_width):
            index += 1
            mosaic_image = get_current_image(images[index].strip(), mosaic_image_width, mosaic_image_height)
            x = -1
            for k in range(i, i + mosaic_image_width, 1):
                x += 1
                y = -1
                for l in range(j, j + mosaic_image_height, 1):
                    y += 1
                    r, g, b, a = mosaic_image.pixelColor(x,y).getRgb()
                    color  = qtg.QColor(r, g, b, a)
                    final_image.setPixelColor(k,l, color)

    pixmap = qtg.QPixmap(final_image)
    return pixmap

def get_current_image(image_source, width, height):
    pixmap = qtg.QPixmap(image_source)
    resolution = qtc.QSize(width, height)
    pixmap = pixmap.scaled(resolution)
    return pixmap.toImage()
