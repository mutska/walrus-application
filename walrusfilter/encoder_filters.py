from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from .CustomDialogs import *


def get_code(c):
    return ord(c)

def get_letter(n):
    return chr(n)


def encode_text_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()
    max_capacity = (width*height) - 16

    msg = settings["text"]

    ascii_codes = [get_code(x) for x in msg]
    binary_rep = [format(x, '08b') for x in ascii_codes]

    text_size = len(msg)
    if (text_size * 8) > max_capacity:
        return pixmap
    k = 0
    idx = 0
    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            if idx < text_size:
                bit = int(binary_rep[idx][k])
                if bit:
                    r = r | (1 << 0)
                else:
                    r = r & (~(1 << 0))
                color  = qtg.QColor(r, g, b,a)
            elif idx < text_size + 8:
                r = r & (~(1 << 0))
                color  = qtg.QColor(r, g, b,a)
            else:
                pixmap = qtg.QPixmap(image)
                return pixmap
            image.setPixelColor(i,j, color)
            k += 1
            if (k == 8):
                k = 0
                idx += 1

    
    pixmap = qtg.QPixmap(image)
    return pixmap

def decode_text_filter(pixmap: qtg.QPixmap):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    decoded_binary = []
    decoded = False
    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            if len(decoded_binary) > 8:
                posible_null = decoded_binary[-8:]
                if all(x == "0" for x in posible_null):
                    decoded = True
                    break
            if r & 1:
                decoded_binary.append("1")
            else:
                decoded_binary.append("0")
        if decoded:
            break;


    raw_binary = ''.join(decoded_binary)
    ascii_codes = [raw_binary[i:i+8] for i in range(0, len(raw_binary), 8)]
    ascii_codes = ascii_codes[:-1]
    int_rep = [int(x, 2) for x in ascii_codes]
    letters = [get_letter(x) for x in int_rep]
    decoded_msg = ''.join(letters)

    filename,  _= qtw.QFileDialog.getSaveFileName(
            None,
            "Selecciona el archivo para guardar...",
            qtc.QDir.homePath(),
            'Text Files (*.txt)'
            )
    if filename:
        try:
            with open(filename, "w+", encoding="utf-8") as text_file:
                text_file.write(decoded_msg)
        except Exception as e:
            qtw.QMessageBox.critical(f"No se pudo guardar el archivo: {e}")

    return pixmap
