from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from screeninfo import get_monitors
from .filters import *
from .convfilters import *
from .letterfilters import *
from .encoder_filters import *
from .CustomDialogs import *
from .photomosaic_filter import *
from .recursive_filter import *
from os import path
import sys


class MainWorker(qtc.QObject):
    finished = qtc.pyqtSignal()

    def __init__(self, pixmap, filter_type, settings):
        super().__init__()
        self.pixmap = pixmap
        self.filter_type = filter_type
        self.settings = settings
    
    def run(self):
        self.chooser(self.filter_type)
        self.finished.emit()

    def chooser(self, filter_type):

        if filter_type == Filter.Gray:
            self.pixmap = gray_filter(self.pixmap)
        elif filter_type == Filter.HighContrast:
            self.pixmap = high_contrast_filter(self.pixmap)
        elif filter_type == Filter.Inverse:
            self.pixmap = inverse_filter(self.pixmap)
        elif filter_type == Filter.Red:
            self.pixmap = red_filter(self.pixmap)
        elif filter_type == Filter.Green:
            self.pixmap = green_filter(self.pixmap)
        elif filter_type == Filter.Blue:
            self.pixmap = blue_filter(self.pixmap)
        elif filter_type == Filter.Brightness:
            self.pixmap = brightness_filter(self.pixmap, self.settings)
        elif filter_type == Filter.Mosaic:
            self.pixmap = mosaic_filter(self.pixmap, self.settings)
        elif filter_type == Filter.HardBlur:
            self.pixmap = hard_blur_filter(self.pixmap)
        elif filter_type == Filter.MotionBlur:
            self.pixmap = motion_blur_filter(self.pixmap)
        elif filter_type == Filter.HorizontalEdges:
            self.pixmap = horizontal_find_edges_filter(self.pixmap)
        elif filter_type == Filter.VerticalEdges:
            self.pixmap = vertical_find_edges_filter(self.pixmap)
        elif filter_type == Filter.DegreesEdges:
            self.pixmap = degrees_find_edges_filter(self.pixmap)
        elif filter_type == Filter.AllEdges:
            self.pixmap = all_edges_filter(self.pixmap)
        elif filter_type == Filter.Sharpen:
            self.pixmap = sharpen_filter(self.pixmap)
        elif filter_type == Filter.HardSharpen:
            self.pixmap = hard_sharpen_filter(self.pixmap)
        elif filter_type == Filter.Emboss:
            self.pixmap = emboss_filter(self.pixmap)
        elif filter_type == Filter.Mean:
            self.pixmap = mean_filter(self.pixmap)
        elif filter_type == Filter.Median:
            self.pixmap = median_filter(self.pixmap)
        elif filter_type == Filter.RGB:
            self.pixmap = rgb_filter(self.pixmap, self.settings)
        elif filter_type == Filter.ONELC:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.ONELG:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.LETTERSWB:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.LETTERSC:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.LETTERSG:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.TEXTC:
            self.pixmap = letters_filter(self.pixmap, self.settings)
        elif filter_type == Filter.CARDS:
            self.pixmap = cards_filter(self.pixmap, self.settings)
        elif filter_type == Filter.WDOMINO:
            self.pixmap = dominos_filter(self.pixmap, self.settings)
        elif filter_type == Filter.BDOMINO:
            self.pixmap = dominos_filter(self.pixmap, self.settings)
        elif filter_type == Filter.TEXTENC:
            self.pixmap = encode_text_filter(self.pixmap, self.settings)
        elif filter_type == Filter.TEXTDEC:
            self.pixmap = decode_text_filter(self.pixmap)
        elif filter_type == Filter.OLEO:
            self.pixmap = oleo_filter(self.pixmap)
        elif filter_type == Filter.CONTRASTAUTO:
            self.pixmap = auto_contrast_filter(self.pixmap)
        elif filter_type == Filter.CLUSTERDITH:
            self.pixmap = cluster_dithering_filter(self.pixmap)
        elif filter_type == Filter.PHOTOMOSAIC:
            self.pixmap = photomosaic_filter(self.pixmap)
        elif filter_type == Filter.RECURSIVE:
            self.pixmap = recursive_filter(self.pixmap, self.settings)
        elif filter_type == Filter.RECURSIVEDIR:
            self.pixmap = preproccesed_recursive_filter(self.pixmap)
        elif filter_type == Filter.PREPROCESSING01:
            self.pixmap = step_one_preprocessing(self.pixmap)
        elif filter_type == Filter.PREPROCESSING02:
            self.pixmap = step_two_preprocessing(self.pixmap, self.settings)

class MainWindow(qtw.QMainWindow):

    def __init__(self):
        """MainWindow constructor.

        This widget will be our main window.
        We'll define all the UI components in here.
        """
        super().__init__()
        self.setWindowTitle('Filter Image Application')


        width, height = self.get_screen_size()

        self.max_size = qtc.QSize(width, height)
        self.resize(width, height)
        self.image = qtg.QImage(
            self.max_size, qtg.QImage.Format_ARGB32)
        self.image.fill(qtg.QColor('black'))

        if getattr(sys, 'frozen', False):
            directory = sys._MEIPASS
        else:
            directory = path.dirname(__file__)
        self.setWindowIcon(qtg.QIcon(path.join(directory, "images", "icon.png")))

        self.build_menu_bar()

        self.image_loaded = False

        mainwidget = qtw.QWidget()
        self.setCentralWidget(mainwidget)
        mainwidget.setLayout(qtw.QVBoxLayout())

        self.pixmap = qtg.QPixmap(self.image)
        self.image_display = qtw.QLabel()
        self.image_display.setPixmap(self.pixmap)
        mainwidget.layout().addWidget(self.image_display)

        self.show()



    def get_screen_size(self):
        width = get_monitors()[0].width
        height = get_monitors()[0].height
        return width, height

    def build_menu_bar(self):

        # Configuracion de la barra de menus
        menubar = self.menuBar()

        # Agrega menus a la barra de menus
        self.build_file_menu(menubar)
        self.build_filters_menu(menubar)
        self.build_convolutional_filters_menu(menubar)
        self.build_letters_filters(menubar)
        self.build_brightness_filter(menubar)
        self.build_steganography_menu(menubar)
        self.build_recursive_menu(menubar)
        self.build_oleo_menu(menubar)
        self.build_automatic_contrast_menu(menubar)
        self.build_dithering_menu(menubar)
        self.build_photomosaic_menu(menubar)
        self.build_restore_menu(menubar)

    def build_file_menu(self, menubar):

        file_menu = menubar.addMenu("Archivos")  

        file_menu.addAction('Abrir Imagen', self.open_image)
        file_menu.addAction('Guardar Imagen', self.save_image)

    def build_filters_menu(self, menubar):

        filter_menu = menubar.addMenu("Filtros")

        filter_menu.addAction("Gris", lambda: self.worker(Filter.Gray))
        filter_menu.addAction("Inverso", lambda: self.worker(Filter.Inverse))
        filter_menu.addAction("Alto Contraste", lambda: self.worker(Filter.HighContrast))
        filter_menu.addAction("Rojo", lambda: self.worker(Filter.Red))
        filter_menu.addAction("Verde", lambda: self.worker(Filter.Green))
        filter_menu.addAction("Azul", lambda: self.worker(Filter.Blue))
        filter_menu.addAction("Mosaico", lambda: self.worker(Filter.Mosaic))
        filter_menu.addAction("Componentes RGB", lambda: self.worker(Filter.RGB))

    def build_letters_filters(self, menubar):

        letter_filter_menu = menubar.addMenu("Filtros con letras")

        letter_filter_menu.addAction("Letra con color", lambda: self.worker(Filter.ONELC))
        letter_filter_menu.addAction("Letra en gris", lambda: self.worker(Filter.ONELG))
        letter_filter_menu.addAction("Letras en blanco y negro", lambda: self.worker(Filter.LETTERSWB))
        letter_filter_menu.addAction("Letras a color", lambda: self.worker(Filter.LETTERSC))
        letter_filter_menu.addAction("Letras en gris", lambda: self.worker(Filter.LETTERSG))
        letter_filter_menu.addAction("Texto personal a color", lambda: self.worker(Filter.TEXTC))
        letter_filter_menu.addAction("Naipes", lambda: self.worker(Filter.CARDS))
        letter_filter_menu.addAction("Dominos (fichas blancas)", lambda: self.worker(Filter.WDOMINO))
        letter_filter_menu.addAction("Dominos (fichas negras)", lambda: self.worker(Filter.BDOMINO))

    def build_convolutional_filters_menu(self, menubar):
        convolutional_filter_menu = menubar.addMenu("Filtros de convolución")
        convolutional_filter_menu.addAction("Blur", lambda: self.worker(Filter.HardBlur))
        convolutional_filter_menu.addAction("Motion Blur", lambda: self.worker(Filter.MotionBlur))
        convolutional_filter_menu.addAction("Bordes Horizontales", lambda: self.worker(Filter.HorizontalEdges))
        convolutional_filter_menu.addAction("Bordes Verticales", lambda: self.worker(Filter.VerticalEdges))
        convolutional_filter_menu.addAction("Bordes 45 Grados", lambda: self.worker(Filter.DegreesEdges))
        convolutional_filter_menu.addAction("Bordes en todas direcciones", lambda: self.worker(Filter.AllEdges))
        convolutional_filter_menu.addAction("Sharpen", lambda: self.worker(Filter.Sharpen))
        convolutional_filter_menu.addAction("Hard Sharpen", lambda: self.worker(Filter.HardSharpen))
        convolutional_filter_menu.addAction("Emboss", lambda: self.worker(Filter.Emboss))
        convolutional_filter_menu.addAction("Promedio", lambda: self.worker(Filter.Mean))
        convolutional_filter_menu.addAction("Mediana", lambda: self.worker(Filter.Median))

    def build_brightness_filter(self, menubar):

        brightness_menu = menubar.addMenu("Brillo")  

        brightness_menu.addAction("Ajustar", lambda: self.worker(Filter.Brightness))


    def build_restore_menu(self, menubar):

        restore_menu = menubar.addMenu("Restaurar")  

        restore_menu.addAction('Imagen Original', self.restore_image)

    def build_steganography_menu(self, menubar):

        encoder_menu = menubar.addMenu("Esteganografía")

        encoder_menu.addAction("Codificar texto", lambda: self.worker(Filter.TEXTENC))
        encoder_menu.addAction("Decodificar texto", lambda: self.worker(Filter.TEXTDEC))

    def build_recursive_menu(self, menubar):

        recursive_menu = menubar.addMenu("Imagenes recursivas")

        recursive_menu.addAction("Tonos de gris", lambda: self.worker(Filter.RECURSIVE))
        recursive_menu.addAction("Directorio con reglas e imagenes preprocesadas", lambda: self.worker(Filter.RECURSIVEDIR))

    def build_oleo_menu(self, menubar):

        oleo_menu = menubar.addMenu("Oleo")

        oleo_menu.addAction("Tonos de gris", lambda: self.worker(Filter.OLEO))

    def build_automatic_contrast_menu(self, menubar):

        contrast_menu = menubar.addMenu("Alto Contraste Automatico")

        contrast_menu.addAction("Tonos de gris", lambda: self.worker(Filter.CONTRASTAUTO))

    def build_dithering_menu(self, menubar):

        dithering_menu = menubar.addMenu("Dithering")

        dithering_menu.addAction("Ordenado", lambda: self.worker(Filter.CLUSTERDITH))

    def build_photomosaic_menu(self, menubar):

        photomosaic_menu = menubar.addMenu("Fotomosaico")

        photomosaic_menu.addAction("Paso 1 Preprocesar directorio de imagenes para obtener su rgb promedio", lambda: self.worker(Filter.PREPROCESSING01))
        photomosaic_menu.addAction("Paso 2 Obtener las mejores imagenes para construir", lambda: self.worker(Filter.PREPROCESSING02))
        photomosaic_menu.addAction("Paso 3 Contruir Fotomosaico", lambda: self.worker(Filter.PHOTOMOSAIC))


    def get_type(self, filter_type):
        
        if filter_type == Filter.ONELC:
            return "ONELC"
        elif filter_type == Filter.ONELG:
            return "ONELG"
        elif filter_type == Filter.LETTERSWB:
            return "WB"
        elif filter_type == Filter.LETTERSC:
            return "LETTERSC"
        elif filter_type == Filter.LETTERSG:
            return "LETTERSG"
        elif filter_type == Filter.TEXTC:
            return "TEXTC"
        elif filter_type == Filter.WDOMINO:
            return "white"
        elif filter_type == Filter.BDOMINO:
            return "black"
        return ""

    def update_dict(self, filter_type, settings: dict):
        image = self.pixmap.toImage()
        width = image.width()
        height = image.height()
        type = self.get_type(filter_type)

        settings["max_width"] = width
        settings["max_height"] = height
        settings["type"] = type



    def worker(self, filter_type):

        if not self.image_loaded:
            return

        settings = {"brightness": 0, # Brightness settings
                    "red": 0, "green": 0, "blue": 0, "check": False, # RGB settings
                    "width": 0, "height" : 0, "max_width": 0, "max_height": 0, # Mosaic
                    "mosaic_width": 0, "mosaic_height" : 0, # and recursive settings
                    "font_size": 1, "letter": "M", "type": "", "text": "", # Letters settings
                    "card_type": ""} # Cards settings

        self.update_dict(filter_type, settings)

        if filter_type == Filter.Brightness:
            brightness_dialog = BrightnessDialog(settings, self)
            brightness_dialog.exec()
        elif filter_type == Filter.RGB:
            rgb_dialog = RGBDialog(settings, self)
            rgb_dialog.exec()
        elif filter_type == Filter.Mosaic:
            mosaic_dialog = MosaicDialog(settings, self)
            mosaic_dialog.exec()
        elif filter_type == Filter.RECURSIVE:
            recursive_dialog = MetricsDialog(settings, self)
            recursive_dialog.exec()
        elif filter_type == Filter.PREPROCESSING02:
            photomosaic_dialog = MetricsDialog(settings, self)
            photomosaic_dialog.exec()
        elif filter_type in {Filter.ONELC, Filter.ONELG, Filter.LETTERSWB, 
                Filter.LETTERSC, Filter.LETTERSG, Filter.TEXTC}:
            letters_dialog = LettersDialog(settings, self)
            letters_dialog.exec()
        elif filter_type == Filter.CARDS:
            settings_dialog = CardsDialog(settings, self)
            settings_dialog.exec()
        elif filter_type in {Filter.BDOMINO, Filter.WDOMINO}:
            dominos_dialog = DominosDialog(settings, self)
            dominos_dialog.exec()
        elif filter_type == Filter.TEXTENC:
            encoder_dialog = TextEncoderDialog(settings, self)
            encoder_dialog.exec()

        self.thread = qtc.QThread()
        self.main_worker = MainWorker(self.pixmap, filter_type, settings)
        self.main_worker.moveToThread(self.thread)

        self.thread.started.connect(self.main_worker.run)
        self.main_worker.finished.connect(self.thread.quit)
        self.main_worker.finished.connect(self.main_worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.start()


        self.thread.finished.connect(
                lambda:  self.set_image()
                )



    def set_image(self):
        self.pixmap = self.main_worker.pixmap
        self.image_display.setPixmap(self.pixmap)
        self.image_display.setAlignment(qtc.Qt.AlignCenter)
        self.completed_message()
    
    def completed_message(self):
        msgBox = qtw.QMessageBox(self)
        msgBox.setWindowTitle("Aviso")
        msgBox.setText("Operación Completada")
        msgBox.exec_()


    def restore_image(self):
        if not self.image_loaded:
            return
        self.build_image()
            
    def open_image(self):
        filename, _ = qtw.QFileDialog.getOpenFileName(
            None, "Selecciona una imagen",
            qtc.QDir.homePath(), "Images (*.png *.xpm *.jpg *.bmp)")
        if filename:
            self.image_source = filename
            self.image_loaded = True
            self.setWindowTitle(qtc.QFileInfo(filename).fileName())
            self.build_image()

    def save_image(self):
        save_file, _ = qtw.QFileDialog.getSaveFileName(
            None, "Guardar imagen",
            qtc.QDir.homePath(), "Images (*.jpg *.bmp)")
        if save_file:
            self.image = self.pixmap.toImage()
            self.image.save(save_file, qtc.QFileInfo(self.image_source).suffix())

    def build_image(self):

        self.image.load(self.image_source)

        if not (self.max_size - self.image.size()).isValid():
            self.image = self.image.scaled(self.max_size, qtc.Qt.KeepAspectRatio)

        self.pixmap = qtg.QPixmap(self.image)
        self.image_display.setPixmap(self.pixmap)
        self.image_display.setAlignment(qtc.Qt.AlignCenter)



