from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

class BrightnessDialog(qtw.QDialog):
    
    def __init__(self, brightness, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Brillo")
        self.brightness = brightness

        self.brightness_value = qtw.QLineEdit()
        self.brightness_value.setValidator(qtg.QIntValidator(-255, 255))
        self.layout().addRow(self.brightness_value)
        
        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        text = self.brightness_value.text()
        value = int(text) if text else 0
        if value <= 255:
            self.brightness["brightness"] = value
        super().accept()

class MosaicDialog(qtw.QDialog):
    
    def __init__(self, mosaic, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Mosaico")
        self.mosaic = mosaic
    
        self.spinbox_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho</h3>"), self.spinbox_width)

        self.spinbox_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura</h3>"), self.spinbox_height)
        
        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.mosaic["width"]  = self.spinbox_width.value()
        self.mosaic["height"] = self.spinbox_height.value()
        super().accept()

class MetricsDialog(qtw.QDialog):
    
    def __init__(self, mosaic, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Mosaico")
        self.mosaic = mosaic
    
        self.spinbox_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho de segmento</h3>"), self.spinbox_width)

        self.spinbox_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura de segmento</h3>"), self.spinbox_height)

        self.spinbox_tile_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho de Tile</h3>"), self.spinbox_tile_width)

        self.spinbox_tile_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.mosaic["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura de Tile</h3>"), self.spinbox_tile_height)
        
        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.mosaic["width"]  = self.spinbox_width.value()
        self.mosaic["height"] = self.spinbox_height.value()
        self.mosaic["mosaic_width"]  = self.spinbox_tile_width.value()
        self.mosaic["mosaic_height"] = self.spinbox_tile_height.value()
        super().accept()

class RGBDialog(qtw.QDialog):

    def __init__(self, rgb, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Componentes RGB")
        self.rgb = rgb

        # Red slider

        self.red_lcd = qtw.QLCDNumber(3)
        self.red_lcd.setStyleSheet("background-color: red")
        self.red_lcd.setSegmentStyle(qtw.QLCDNumber.Filled)
        self.red_lcd.display(0)
        self.red_lcd.setDecMode()

        self.red_sld = qtw.QSlider(qtc.Qt.Horizontal)
        self.red_sld.setRange(0, 255)
        self.red_sld.setTickPosition(qtw.QSlider.TicksBothSides)
        self.red_sld.setTickInterval(20)
        self.red_sld.valueChanged.connect(self.red_sld_changed)

        self.layout().addRow(qtw.QLabel("<h3>Rojo</h3>"))
        self.layout().addRow(self.red_lcd)
        self.layout().addRow(self.red_sld)

        # Green slider

        self.green_lcd = qtw.QLCDNumber(3)
        self.green_lcd.setStyleSheet("background-color: green")
        self.green_lcd.setSegmentStyle(qtw.QLCDNumber.Filled)
        self.green_lcd.display(0)
        self.green_lcd.setDecMode()

        self.green_sld = qtw.QSlider(qtc.Qt.Horizontal)
        self.green_sld.setRange(0, 255)
        self.green_sld.setTickPosition(qtw.QSlider.TicksBothSides)
        self.green_sld.setTickInterval(20)
        self.green_sld.valueChanged.connect(self.green_sld_changed)

        self.layout().addRow(qtw.QLabel("<h3>Verde</h3>"))
        self.layout().addRow(self.green_lcd)
        self.layout().addRow(self.green_sld)

        # Blue slider

        self.blue_lcd = qtw.QLCDNumber(3)
        self.blue_lcd.setStyleSheet("background-color: blue")
        self.blue_lcd.setSegmentStyle(qtw.QLCDNumber.Filled)
        self.blue_lcd.display(0)
        self.blue_lcd.setDecMode()

        self.blue_sld = qtw.QSlider(qtc.Qt.Horizontal)
        self.blue_sld.setRange(0, 255)
        self.blue_sld.setTickPosition(qtw.QSlider.TicksBothSides)
        self.blue_sld.setTickInterval(20)
        self.blue_sld.valueChanged.connect(self.blue_sld_changed)

        self.layout().addRow(qtw.QLabel("<h3>Azul</h3>"))
        self.layout().addRow(self.blue_lcd)
        self.layout().addRow(self.blue_sld)

        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

        self.red = 0
        self.green = 0
        self.blue = 0

    def accept(self):
        self.rgb["red"] = self.red
        self.rgb["green"] = self.green
        self.rgb["blue"] = self.blue
        self.rgb["check"] = True
        super().accept()

    def red_sld_changed(self, value):
        self.red_lcd.display(value)
        self.red_sld.setValue(value)
        self.red = value

    def green_sld_changed(self, value):
        self.green_lcd.display(value)
        self.green_sld.setValue(value)
        self.green = value

    def blue_sld_changed(self, value):
        self.blue_lcd.display(value)
        self.blue_sld.setValue(value)
        self.blue = value


class LettersDialog(qtw.QDialog):

    def __init__(self, settings, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Configuración")
        self.settings = settings

        self.spinbox_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho</h3>"), self.spinbox_width)

        self.spinbox_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura</h3>"), self.spinbox_height)

        if self.settings["type"] == "TEXTC":
            self.line_edit = qtw.QLineEdit(clearButtonEnabled=True)
            self.layout().addRow(qtw.QLabel("<h3>Texto</h3>"), self.line_edit)
        elif self.settings["type"] in {"ONELC" , "ONELG"}:
            self.combobox = qtw.QComboBox()
            letters = ["M", "L", "C"]
            self.combobox.addItems(letters)
            self.layout().addRow(qtw.QLabel("<h3>Letra</h3>"), self.combobox)

        self.spinbox_font_size = qtw.QSpinBox(value=1, minimum=1, maximum=35, singleStep=5)
        self.layout().addRow(qtw.QLabel("<h3>Tamaño de fuente</h3>"), self.spinbox_font_size)

        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.settings["width"]  = self.spinbox_width.value()
        self.settings["height"] = self.spinbox_height.value()
        self.settings["font_size"] = self.spinbox_font_size.value()

        if self.settings["type"] == "TEXTC":
            self.settings["text"] = self.line_edit.text()
        elif self.settings["type"] in {"ONELC" , "ONELG"}:
            self.settings["letter"] = self.combobox.currentText()

        super().accept()

class CardsDialog(qtw.QDialog):

    def __init__(self, settings, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Configuración")
        self.settings = settings

        self.spinbox_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho</h3>"), self.spinbox_width)

        self.spinbox_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura</h3>"), self.spinbox_height)

        self.combobox = qtw.QComboBox()
        letters = ["Corazones", "Tréboles", "Diamantes", "Picas"]
        self.combobox.addItems(letters)
        self.layout().addRow(qtw.QLabel("<h3>Tipo de carta</h3>"), self.combobox)

        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.settings["width"]  = self.spinbox_width.value()
        self.settings["height"] = self.spinbox_height.value()
        self.settings["card_type"] = self.combobox.currentText()

        super().accept()

class DominosDialog(qtw.QDialog):

    def __init__(self, settings, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Configuración")
        self.settings = settings

        self.spinbox_width = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_width"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Ancho</h3>"), self.spinbox_width)

        self.spinbox_height = qtw.QSpinBox(value=1, minimum=1, maximum=self.settings["max_height"], singleStep=10)
        self.layout().addRow(qtw.QLabel("<h3>Altura</h3>"), self.spinbox_height)

        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.settings["width"]  = self.spinbox_width.value()
        self.settings["height"] = self.spinbox_height.value()
        super().accept()

class TextEncoderDialog(qtw.QDialog):

    def __init__(self, settings, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.setWindowTitle("Configuración")
        self.settings = settings

        self.line_edit = qtw.QTextEdit()
        self.layout().addRow(qtw.QLabel("<h3>Mensaje</h3>"), self.line_edit)

        self.accept_btn = qtw.QPushButton("Aceptar", clicked=self.accept)
        self.cancel_btn = qtw.QPushButton("Cancelar", clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        self.settings["text"] = self.line_edit.toPlainText()
        super().accept()
