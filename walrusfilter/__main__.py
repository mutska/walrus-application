from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from .mainwindow import MainWindow
from os import path
import sys

def main():
    app = qtw.QApplication(sys.argv)
    mw = MainWindow()
    # Stylesheet config
    if getattr(sys, 'frozen', False):
        directory = sys._MEIPASS
    else:
        directory = path.dirname(__file__)
    stylesheet = open(path.join(directory, "style" , "stylesheet.qss"), "r")
    style = stylesheet.read()
    
    app.setStyleSheet(style)

    # Font Config
    app.setFont(qtg.QFont("Franklin Gothic", 10))
    sys.exit(app.exec())


if __name__ == '__main__':
    main()

