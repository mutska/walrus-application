from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from .CustomDialogs import *


def hard_blur_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += r
            green += g
            blue += b

            # 2 Pixel (0, -2)
            r, g, b, a = image.pixelColor(i - 2, j).getRgb()
            red += r
            green += g
            blue += b

            # 3 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 4 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 5 Pixel (0, 2)
            r, g, b, a = image.pixelColor(i + 2, j).getRgb()
            red += r
            green += g
            blue += b

            # 6 Pixel (-2, 0)
            r, g, b, a = image.pixelColor(i, j - 2).getRgb()
            red += r
            green += g
            blue += b

            # 7 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 8 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 9 Pixel (2, 0)
            r, g, b, a = image.pixelColor(i, j + 2).getRgb()
            red += r
            green += g
            blue += b

            # 10 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 11 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 12 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 13 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            red = red // 13
            green = green // 13
            blue = blue // 13


            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def motion_blur_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 9
    filter_heigth = 9

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += r
            green += g
            blue += b

            # 2 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 3 Pixel (-2, -2)
            r, g, b, a = image.pixelColor(i - 2, j - 2).getRgb()
            red += r
            green += g
            blue += b

            # 4 Pixel (-3, -3)
            r, g, b, a = image.pixelColor(i - 3, j - 3).getRgb()
            red += r
            green += g
            blue += b

            # 5 Pixel (-4, -4)
            r, g, b, a = image.pixelColor(i - 4, j - 4).getRgb()
            red += r
            green += g
            blue += b

            # 6 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 7 Pixel (2, 2)
            r, g, b, a = image.pixelColor(i + 2, j + 2).getRgb()
            red += r
            green += g
            blue += b

            # 8 Pixel (3, 3)
            r, g, b, a = image.pixelColor(i + 3, j + 3).getRgb()
            red += r
            green += g
            blue += b

            # 9 Pixel (4, 4)
            r, g, b, a = image.pixelColor(i + 4, j + 4).getRgb()
            red += r
            green += g
            blue += b


            red = red // 9
            green = green // 9
            blue = blue // 9


            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap


def horizontal_find_edges_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += (2*r)
            green += (2*g)
            blue += (2*b)

            # 2 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 3 Pixel (-2, 0)
            r, g, b, a = image.pixelColor(i, j - 2).getRgb()
            red -= r
            green -= g
            blue -= b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def vertical_find_edges_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += (4*r)
            green += (4*g)
            blue += (4*b)

            # 2 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 3 Pixel (-2, 0)
            r, g, b, a = image.pixelColor(i, j - 2).getRgb()
            red -= r
            green -= g
            blue -= b

            # 4 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 5 Pixel (2, 0)
            r, g, b, a = image.pixelColor(i, j + 2).getRgb()
            red -= r
            green -= g
            blue -= b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def degrees_find_edges_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += (6*r)
            green += (6*g)
            blue += (6*b)

            # 2 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red -= (2*r)
            green -= (2*g)
            blue -= (2*b)

            # 3 Pixel (-2, -2)
            r, g, b, a = image.pixelColor(i - 2, j - 2).getRgb()
            red -= r
            green -= g
            blue -= b

            # 4 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red -= (2*r)
            green -= (2*g)
            blue -= (2*b)

            # 5 Pixel (2, 2)
            r, g, b, a = image.pixelColor(i + 2, j + 2).getRgb()
            red -= r
            green -= g
            blue -= b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def all_edges_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 3
    filter_heigth = 3

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += (8*r)
            green += (8*g)
            blue += (8*b)

            # 2 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red -= r
            green -= g
            blue -= b

            # 3 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red -= r
            green -= g
            blue -= b

            # 4 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 5 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 6 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 7 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 8 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 9 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def sharpen_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 3
    filter_heigth = 3

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += (9*r)
            green += (9*g)
            blue += (9*b)

            # 2 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red -= r
            green -= g
            blue -= b

            # 3 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red -= r
            green -= g
            blue -= b

            # 4 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 5 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 6 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 7 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 8 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 9 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red -= r
            green -= g
            blue -= b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def hard_sharpen_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 3
    filter_heigth = 3

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red -= (7*r)
            green -= (7*g)
            blue -= (7*b)

            # 2 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 3 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 4 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 5 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 6 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 7 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 8 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 9 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def emboss_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 3
    filter_heigth = 3

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)
    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0


            # 1 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red -= r
            green -= g
            blue -= b

            # 2 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 3 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 4 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 5 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red -= r
            green -= g
            blue -= b

            # 6 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # Optional Bias
            red += 128
            green += 128
            blue += 128


            red = red if red >= 0  else 0
            red = red if red <= 255  else 255

            green = green if green >= 0  else 0
            green = green if green <= 255  else 255

            blue = blue if blue >= 0  else 0
            blue = blue if blue <= 255  else 255

            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap


def mean_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    
    
    
    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = 0
            green = 0
            blue = 0

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red += r
            green += g
            blue += b


            # 2 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 3 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red += r
            green += g
            blue += b

            # 4 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 5 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red += r
            green += g
            blue += b


            # 6 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 7 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            # 8 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red += r
            green += g
            blue += b

            # 9 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red += r
            green += g
            blue += b

            red = red // 9
            green = green // 9
            blue = blue // 9


            color  = qtg.QColor(red, green, blue,a)
            copy.setPixelColor(i,j, color)


    pixmap = qtg.QPixmap(copy)
    return pixmap

def median_filter(pixmap: qtg.QPixmap):

    copy = pixmap.copy().toImage()
    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    filter_width = 5
    filter_heigth = 5

    x_start = filter_width // 2
    y_start = filter_heigth // 2

    x_stop = width - (filter_width // 2)
    y_stop = height - (filter_heigth // 2)

    

    for i in range(x_start, x_stop):
        for j in range(y_start, y_stop):

            red = []
            green = []
            blue = []

            # 1 Pixel (0, 0)
            r, g, b, a = image.pixelColor(i, j).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 2 Pixel (0, -1)
            r, g, b, a = image.pixelColor(i - 1, j).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 3 Pixel (0, 1)
            r, g, b, a = image.pixelColor(i + 1, j).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 4 Pixel (-1, 0)
            r, g, b, a = image.pixelColor(i, j - 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 5 Pixel (1, 0)
            r, g, b, a = image.pixelColor(i, j + 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)


            # 6 Pixel (-1, -1)
            r, g, b, a = image.pixelColor(i - 1, j - 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 7 Pixel (1, 1)
            r, g, b, a = image.pixelColor(i + 1, j + 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 8 Pixel (-1, 1)
            r, g, b, a = image.pixelColor(i + 1, j - 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            # 9 Pixel (1, -1)
            r, g, b, a = image.pixelColor(i - 1, j + 1).getRgb()
            red.append(r)
            green.append(g)
            blue.append(b)

            red.sort()
            green.sort()
            blue.sort()

            r = red[4]
            g = green[4]
            b = blue[4]

            color  = qtg.QColor(r, g, b,a)
            copy.setPixelColor(i,j, color)

    pixmap = qtg.QPixmap(copy)
    return pixmap

