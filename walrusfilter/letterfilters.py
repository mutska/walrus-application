from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from .CustomDialogs import *


def rgb_to_hex(r, g, b):
    return ('#{:X}{:X}{:X}').format(r, g, b)

def get_letter(value):

    if 0 <= value <= 15:
        return "M"
    if 16 <= value <= 31:
        return "N"
    if 32 <= value <= 47:
        return "H"
    if 48 <= value <= 63:
        return "#"
    if 64 <= value <= 79:
        return "Q"
    if 80 <= value <= 95:
        return "U"
    if 96 <= value <= 111:
        return "A"
    if 112 <= value <= 127:
        return "D"
    if 128 <= value <= 143:
        return "O"
    if 144 <= value <= 159:
        return "Y"
    if 160 <= value <= 175:
        return "2"
    if 176 <= value <= 191:
        return "$"
    if 192 <= value <= 209:
        return "%"
    if 210 <= value <= 225:
        return "+"
    if 226 <= value <= 239:
        return "."
    if 240 <= value <= 255:
        return " "



def get_card_letter(value, card_type):

    index = 0
    if card_type == "Corazones":
        index = 0
    elif card_type == "Tréboles":
        index = 1
    elif card_type == "Diamantes":
        index = 2
    elif card_type == "Picas":
        index = 3

    if 0 <= value <= 20:
        letters = "MZmz"
        return letters[index]
    elif 21 <= value <= 40:
        letters = "LYly"
        return letters[index]
    elif 41 <= value <= 60:
        letters = "KZkx"
        return letters[index]
    elif 61 <= value <= 80:
        letters = "JWjw"
        return letters[index]
    elif 81 <= value <= 100:
        letters = "IViv"
        return letters[index]
    elif 101 <= value <= 120:
        letters = "HUhu"
        return letters[index]
    elif 121 <= value <= 140:
        letters = "GTgt"
        return letters[index]
    elif 141 <= value <= 160:
        letters = "FSfs"
        return letters[index]
        return "D"
    elif 161 <= value <= 180:
        letters = "ERer"
        return letters[index]
    elif 181 <= value <= 200:
        letters = "DQdq"
        return letters[index]
    elif 201 <= value <= 220:
        letters = "CPcp"
        return letters[index]
    elif 221 <= value <= 240:
        letters = "BObo"
        return letters[index]
    elif 241 <= value <= 255:
        letters = "ANan"
        return letters[index]

def get_domino_letter(value, type):

    if 0 <= value <= 36:
        return "6^" if type == "white" else "0)"
    elif 37 <= value <= 72:
        return "5%" if type == "white" else "1!"
    elif 73 <= value <= 108:
        return "4$" if type == "white" else "2@"
    elif 109 <= value <= 144:
        return "3#"
    elif 145 <= value <= 180:
        return "2@" if type == "white" else "4$"
    elif 181 <= value <= 216:
        return "1!" if type == "white" else "5%"
    elif 217 <= value <= 255:
        return "0)" if type == "white" else "6^"

def letters_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()

    type = settings["type"]

    custom_text = settings["text"]
    text_size = len(custom_text)
    index = 0

    if settings["width"] == 0 or settings["height"] == 0:
        return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
        return pixmap

    mosaic_width = settings["width"]
    mosaic_height = settings["height"]

    html_file = ""
    preamble = """<!DOCTYPE html>
    <html>
    <head>
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            line-height: 18px;
          }
    </style>
    </head>\n"""
    prefix = "\t<pre>\n"
    suffix = "\t</pre>\n</html>"

    content_prefix = "<font size=\""
    content_prefix += str(settings["font_size"])
    content_prefix += "\" face=\"courier\" color=\""

    content_letter = settings["letter"]
    content_suffix = "</font>"

    html_file += preamble
    html_file += prefix


    for j in range(0, height, mosaic_height):
        for i in range(0, width, mosaic_width):
            r_average_color = 0
            g_average_color = 0
            b_average_color = 0
            average_gray = 0
            size = 0
            alpha = 0
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        size += 1
                        r, g, b, alpha = image.pixelColor(k,l).getRgb()
                        if type == "LETTERSC":
                            gray = (r + g + b) // 3
                            average_gray += gray
                            r_average_color += r
                            g_average_color += g
                            b_average_color += b
                        if type == "ONELG" or type == "WB" or type == "LETTERSG":
                            gray = (r + g + b) // 3
                            average_gray += gray
                        elif type == "ONELC" or type == "TEXTC":
                            r_average_color += r
                            g_average_color += g
                            b_average_color += b

            average_gray = average_gray // size
            r_average_color = r_average_color // size
            g_average_color = g_average_color // size
            b_average_color = b_average_color // size

            # Build the color
            content_color = ""
            if type == "WB":
                content_color += "#000000"
            if type == "ONELG" or type == "LETTERSG":
                content_color += rgb_to_hex(average_gray, average_gray, average_gray)
            else:
                content_color += rgb_to_hex(r_average_color, g_average_color, b_average_color)

            content_color += "\">"

            # Write the letter
            html_file += content_prefix
            html_file += content_color
            if type == "WB" or type == "LETTERSC" or  type == "LETTERSG":
                html_file += str(get_letter(average_gray))
            elif type == "TEXTC":
                html_file += custom_text[index]
                index = (index + 1) % text_size
            else:
                html_file += content_letter
            html_file += content_suffix


        html_file += "\n"

    html_file += suffix
    filename,  _= qtw.QFileDialog.getSaveFileName(
            None,
            "Selecciona el archivo para guardar...",
            qtc.QDir.homePath(),
            'Html Files (*.html)'
            )
    if filename:
        try:
            with open(filename, "w+", encoding="utf-8") as html_image_file:
                html_image_file.write(html_file)
        except Exception as e:
            qtw.QMessageBox.critical(f"No se pudo guardar el archivo: {e}")
    return pixmap

def cards_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    if settings["width"] == 0 or settings["height"] == 0:
        return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
        return pixmap

    mosaic_width = settings["width"]
    mosaic_height = settings["height"]

    text_file = ""
    card_type = settings["card_type"]

    for j in range(0, height, mosaic_height):
        for i in range(0, width, mosaic_width):
            average_gray = 0
            size = 0
            alpha = 0
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        size += 1
                        r, g, b, alpha = image.pixelColor(k,l).getRgb()
                        gray = (r + g + b) // 3
                        average_gray += gray

            average_gray = average_gray // size


            text_file += str(get_card_letter(average_gray, card_type))


        text_file += "\n"

    filename,  _= qtw.QFileDialog.getSaveFileName(
            None,
            "Selecciona el archivo para guardar...",
            qtc.QDir.homePath(),
            'Text Files (*.txt)'
            )
    if filename:
        try:
            with open(filename, "w+", encoding="utf-8") as cards_file:
                cards_file.write(text_file)
        except Exception as e:
            qtw.QMessageBox.critical(f"No se pudo guardar el archivo: {e}")
    return pixmap

def dominos_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    if settings["width"] == 0 or settings["height"] == 0:
        return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
        return pixmap

    mosaic_width = settings["width"]
    mosaic_height = settings["height"]
    type = settings["type"]

    text_file = ""

    for j in range(0, height, mosaic_height):
        for i in range(0, width, mosaic_width):
            average_gray = 0
            size = 0
            alpha = 0
            for k in range(i, i + mosaic_width, 1):
                for l in range(j, j + mosaic_height, 1):
                    if k < width and l < height:
                        size += 1
                        r, g, b, alpha = image.pixelColor(k,l).getRgb()
                        gray = (r + g + b) // 3
                        average_gray += gray

            average_gray = average_gray // size


            text_file += str(get_domino_letter(average_gray, type))


        text_file += "\n"

    filename,  _= qtw.QFileDialog.getSaveFileName(
            None,
            "Selecciona el archivo para guardar...",
            qtc.QDir.homePath(),
            'Text Files (*.txt)'
            )
    if filename:
        try:
            with open(filename, "w+", encoding="utf-8") as dominos_file:
                dominos_file.write(text_file)
        except Exception as e:
            qtw.QMessageBox.critical(f"No se pudo guardar el archivo: {e}")
    return pixmap
