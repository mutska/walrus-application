from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
import os
import sys
from math import dist

def get_image_match(dir_path, gray):
    brightness = 127
    for i in range(0, 30):
        if gray >= brightness - 8*i:
            file_image = dir_path + "/" + str(i + 1) + "r.jpg"
            return file_image
    file_image = dir_path + "/30r.jpg"
    return file_image


def build_image(image: qtg.QImage, original, resized, constant, mosaic_image_width, mosaic_image_height):

    width = image.width()
    height = image.height()

    brightness = 127 - constant

    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            rr = r + brightness
            gg = g + brightness
            bb = b + brightness
            if rr < 0: rr = 0
            elif rr > 255: rr = 255
            if gg < 0: gg = 0
            elif gg > 255: gg = 255
            if bb < 0: bb = 0
            elif bb > 255: bb = 255
            color  = qtg.QColor(rr, gg, bb ,a)
            image.setPixelColor(i,j, color)
    image.save(original)
    resize_image(image.copy(), resized, mosaic_image_width, mosaic_image_height)

def resize_image(image, resized, width, height):
    resolution = qtc.QSize(width, height)
    image.scaled(resolution).save(resized)


def recursive_filter(pixmap: qtg.QPixmap, settings: dict):

    image = pixmap.toImage()
    width = image.width()
    height = image.height()


    if settings["width"] == 0 or settings["height"] == 0:
         return pixmap

    if settings["width"] == 1 and settings["height"] == 1:
         return pixmap

    mosaic_width = settings["width"]
    mosaic_height = settings["height"]
    mosaic_image_width = settings["mosaic_width"]
    mosaic_image_height = settings["mosaic_height"]

    options = qtw.QFileDialog.Options()
    options |= qtw.QFileDialog.ShowDirsOnly
    dir_path = qtw.QFileDialog.getExistingDirectory(None, "Directorio de imagenes", options=options)

    constructor_source = dir_path + "/" + "constructor.txt"
    metrics_source = dir_path + "/" + "metrics.txt"
    for i in range(width):
        for j in range(height):                    
            r, g, b, a = image.pixelColor(i,j).getRgb()
            gray = (r + g + b) // 3
            color  = qtg.QColor(gray, gray, gray,a)
            image.setPixelColor(i,j, color)


    y_mosaics = height // mosaic_height
    x_mosaics = width // mosaic_width

    for i in range(0, 30):
        filename_original = dir_path + "/" + str(i + 1) + ".jpg"
        filename_resized = dir_path + "/" + str(i + 1) + "r.jpg"
        constant = 8*i
        build_image(image.copy(),
                filename_original,
                filename_resized,
                constant,
                mosaic_image_width,
                mosaic_image_height)


    mosaic_area = mosaic_height * mosaic_width
    constructor = open(constructor_source, "w+", encoding="utf-8")
    for j in range(0, height, mosaic_height):
        for i in range(0, width, mosaic_width):
            if j + mosaic_height <= height and i + mosaic_width <= width:
                gray_average = 0
                for k in range(i, i + mosaic_width, 1):
                    for l in range(j, j + mosaic_height, 1):
                        r, g, b, _ = image.pixelColor(k,l).getRgb()
                        gray_average += (r + g + b) // 3
                gray_average = gray_average // mosaic_area
                best_image_source = get_image_match(dir_path, gray_average)
                constructor.write(f"{best_image_source}\n")
    constructor.close()

    final_image_width = x_mosaics * mosaic_image_width
    final_image_height = y_mosaics * mosaic_image_height
    final_image = qtg.QImage(qtc.QSize(final_image_width, final_image_height), qtg.QImage.Format_ARGB32)

    metrics = open(metrics_source, "w+", encoding="utf-8")
    metrics.write(f"{width}\n")
    metrics.write(f"{height}\n")
    metrics.write(f"{mosaic_width}\n")
    metrics.write(f"{mosaic_height}\n")
    metrics.write(f"{mosaic_image_width}\n")
    metrics.write(f"{mosaic_image_height}\n")
    metrics.close()

    constructor = open(constructor_source, "r", encoding="utf-8")
    images = constructor.readlines()
    constructor.close()
    index = -1

    for j in range(0, final_image_height, mosaic_image_height):
        for i in range(0, final_image_width, mosaic_image_width):
            index += 1
            mosaic_image = get_current_image(images[index].strip())
            x = -1
            for k in range(i, i + mosaic_image_width, 1):
                x += 1
                y = -1
                for l in range(j, j + mosaic_image_height, 1):
                    y += 1
                    r, g, b, a = mosaic_image.pixelColor(x,y).getRgb()
                    color  = qtg.QColor(r, g, b, a)
                    final_image.setPixelColor(k,l, color)

    pixmap = qtg.QPixmap(final_image)
    return pixmap

def get_current_image(image_source):
    return qtg.QImage(image_source)


def preproccesed_recursive_filter(pixmap: qtg.QPixmap):

    options = qtw.QFileDialog.Options()
    options |= qtw.QFileDialog.ShowDirsOnly
    dir_path = qtw.QFileDialog.getExistingDirectory(None, "Directorio de reglas e imagenes preprocesadas", options=options)

    constructor_source = dir_path + "/" + "constructor.txt"
    metrics_source = dir_path + "/" + "metrics.txt"

    if os.path.exists(constructor_source) == False or os.path.exists(metrics_source) == False:
        return pixmap

    if not any((file.endswith(".JPG") or file.endswith(".jpg")) for file in os.listdir(dir_path)):
        return pixmap

    metrics = open(metrics_source, "r", encoding="utf-8")
    metrics_list = metrics.readlines()
    metrics.close()

    width = int(metrics_list[0].strip())
    height = int(metrics_list[1].strip())

    mosaic_width = int(metrics_list[2].strip())
    mosaic_height = int(metrics_list[3].strip())

    mosaic_image_width = int(metrics_list[4].strip())
    mosaic_image_height = int(metrics_list[5].strip())

    y_mosaics = height // mosaic_height
    x_mosaics = width // mosaic_width


    final_image_width = x_mosaics * mosaic_image_width
    final_image_height = y_mosaics * mosaic_image_height
    final_image = qtg.QImage(qtc.QSize(final_image_width, final_image_height), qtg.QImage.Format_ARGB32)

    constructor = open(constructor_source, "r", encoding="utf-8")
    images = constructor.readlines()
    constructor.close()
    index = -1

    for j in range(0, final_image_height, mosaic_image_height):
        for i in range(0, final_image_width, mosaic_image_width):
            index += 1
            mosaic_image = get_current_image(images[index].strip())
            x = -1
            for k in range(i, i + mosaic_image_width, 1):
                x += 1
                y = -1
                for l in range(j, j + mosaic_image_height, 1):
                    y += 1
                    r, g, b, a = mosaic_image.pixelColor(x,y).getRgb()
                    color  = qtg.QColor(r, g, b, a)
                    final_image.setPixelColor(k,l, color)

    pixmap = qtg.QPixmap(final_image)
    return pixmap
